// @ts-check
import kitten from '@small-web/kitten'

export default ({ socket }) => {
  let postIndex = 0

  const sendNextMessage = () => {
    // See https://docs.joinmastodon.org/methods/streaming/#events
    if (kitten.db.posts.length > 0) {
      if (kitten.db.posts[postIndex] !== undefined) {
        const message = JSON.stringify({
          event: 'update',
          payload: JSON.stringify(kitten.db.posts[postIndex])
        })
        socket.send(message)
        postIndex = ++postIndex > kitten.db.posts.length - 1 ? 0 : postIndex
      } else {
        // This post no longer exists, it must have been
        // removed in the admin while the app was running.
        // Go back to the start.
        postIndex = 0
      }
    }
    setTimeout(sendNextMessage, Math.random()*2000+1000)
  }

  setTimeout(sendNextMessage, 1000)
}
