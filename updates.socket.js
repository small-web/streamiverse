import Post from './Post.component.js'

let stream = null

export default function socket ({ socket }) {
  // Lazily start listening for timeline updates on first request.
  if (stream === null) {
    console.log('  🐘 Listening for Mastodon updates…')
    stream = new kitten.WebSocket(`wss://${kitten.domain}/stream.socket`)

    stream.addEventListener('message', event => {
      const message = JSON.parse(event.data)

      if (message.event === 'update') {
        const post = JSON.parse(message.payload)

        // console.log(`  🐘 Got an update from ${post.account.username}!`)

        const update = kitten.html`
          <div swap-target="afterbegin:#posts">
            <${Post} post=${post} />
          </div>
        `

        socket.all(update)
      }
    })
  }
}
