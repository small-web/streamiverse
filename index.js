/**
  This is a Kitten app. Display a helpful message if it is run directly via Node.js.
*/
// @ts-check

const format = {
  ansi: ((code, text) => `\u001b[${code}m${text}\u001b[0m`),
  bold: text => format.ansi(1, text),
  italic: text => format.ansi(3, text),
  underline: text => format.ansi(4, text)
}

console.error(
`😺 ${format.bold('This is a Small Web app powered by Kitten.')}

   ${format.underline('To run it:')}

1. Install Kitten

   ${format.italic('https://codeberg.org/kitten/app#install-kitten')}

2. Run Kitten from this folder:

   ${format.italic('kitten')}

 ╭───────────────────────────────────────────╮
 │ Like this? Fund us!                       │
 │                                           │
 │ We’re a tiny, independent not-for-profit. │
 │ https://small-tech.org/fund-us            │
 ╰───────────────────────────────────────────╯
`)
