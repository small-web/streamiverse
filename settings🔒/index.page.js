import Post, { RemoveButton } from '../Post.component.js'

const AddButton = ({ busy = false }) => kitten.html`
  <button id='addButton' ${busy && 'disable'}>Add${busy && 'ing…'}</button>
`

const PostLinkInput = () => kitten.html`
  <input
    id='post-url'
    type='url'
    name='postUrl'
    placeholder='e.g., https://mastodon.ar.al/@aral/111572057761839773'
    required
  ></input>
`

const RemovalRequestAccountInput = () => kitten.html`
  <input
    id='removal-request-account'
    name='removalRequestAccount'
    type='url'
    value='${kitten.db.settings.removalRequestAccount}'
    connect
    trigger='input delay:200ms'
  ></input>
`

const SavedMessage = () => kitten.html`
  <div id='saved' morph='settle:1.5s' aria-live='assertive'>Saved!</div>
`

const StatusMessage = ({ SLOT, type='error' }) => kitten.html`
  <div id='status-message' class='${type}'>
    ${SLOT}
  </div>
`

const PostsList = ({ SLOT }) => kitten.html`
  <ul id='posts' hx-swap-oob='afterbegin'>
    ${SLOT}
  </ul>
`

const PostCountTitle = () => kitten.html`
  <markdown>
    ## View and remove posts (${kitten.db.posts.length})
  </markdown>
`

// Create the database table if it doesn’t already exist.
if (kitten.db.posts === undefined) kitten.db.posts = []

/**
  Page.
*/
export default () => kitten.html`
  <page water>
  <markdown>
    # Streamiverse settings

    Browse and curate your stream. 

    ## Add to stream
  </markdown>

  <div>
    <form name='add' connect>
      <label for='postUrl'>Post link:</label>
      <${PostLinkInput} />
      <${AddButton} />
      <${StatusMessage} />
    </form>

    <details id='hint'>
      <summary>Help</summary>
      <p>Copy the post link from the context menu on the date of the post.</p>
      <img class='thumb' src='./copy-link.png' alt='Screenshot of Mastodon interface showing the context menu open on a post’s link with the Copy Link option showing.'>
    </details>

    <h2>Moderation</h2>
    
    <form name='moderation'>
      <label for='removalRequestAccount'>Fediverse account for removal requests:</label>
      <${RemovalRequestAccountInput} />
      <${SavedMessage} />
    </form>

    <${PostCountTitle} />

    <form>
      <${PostsList}>
        ${kitten.db.posts.map(post => kitten.html`<${Post} post=${post} showRemoveButton />`)}
      </>
    </form>
  </div>

  <style>
    label { display: block; }
    input, button {
      display: inline-block;
    }
    input {
      width: 42ch;
    }
    ul {
      padding: 0;
    }
    #saved.settling { opacity: 1; }
    #saved { opacity: 0; color: var(--text-muted); display: inline-block; padding: 10px; transition: opacity 0.3s ease-in; }
  </style>
`

/**
  When an onConnect() handler is exported from a page, Kitten
  automatically creates a WebSocket route and wires it up to
  call the handler with a page (PageSocket instance).

  When an HTMX request is triggered, Kitten maps the name of
  the triggering DOM element to an event handler for the event
  with the same name that you listen for on that page reference.

  As you are handling the workload of a call on the server,
  you can stream updates to the page by sending snippets of
  HTML formatted using HTMX conventions.
*/
export function onConnect ({ page }) {
  page.on('removalRequestAccount', data => {
    kitten.db.settings.removalRequestAccount = data.removalRequestAccount
    page.send(kitten.html`<${SavedMessage} />`)
  })

  page.on('add', async data => {
    // Show status update.
    page.send(kitten.html`<${AddButton} busy/>`)

    // Calculate the status API URL and fetch the post.
    const postUrl = new URL(data.postUrl)
    const fediverseServer = postUrl.origin
    const postId = postUrl.pathname.substring(postUrl.pathname.lastIndexOf('/') + 1)
    const statusApiUrl = `${fediverseServer}/api/v1/statuses/${postId}`
    
    let response
    let post
    try {
      response = await fetch(statusApiUrl)
      post = await response.json()
    } catch (error) {
      page.send(kitten.html`
        <${StatusMessage}>Adding post failed. (${error.message})</>
      `)
      page.send(kitten.html`<${PostLinkInput} />`)
      page.send(kitten.html`<${AddButton} />`)
      return
    }

    if (response.status !== 200) {
      page.send(kitten.html`
        <${StatusMessage}>Adding post failed. (Status code: ${response.status})</>
      `)
      page.send(kitten.html`<${PostLinkInput} />`)
      page.send(kitten.html`<${AddButton} />`)
      return
    }

    // Persist the post.
    kitten.db.posts.push(post)

    // Clear URL input (replace it with a fresh one).
    page.send(kitten.html`<${PostLinkInput} />`)

    // Re-enable the Add button.
    page.send(kitten.html`<${AddButton} />`)

    // Update post count in title.
    page.send(kitten.html`<${PostCountTitle} />`)
    
    // Ensure any existing status message is reset.
    page.send(kitten.html`<${StatusMessage} />`)

    // Show the new post at the top of the list.
    page.send(kitten.html`
      <${PostsList}>
        <${Post} post=${post} showRemoveButton />
      </>
    `)
  })

  page.on('remove', data => {
    // Disable delete button.
    page.send(kitten.html`<${RemoveButton} disabled postId=${data.remove} />`)

    // Find post.
    const postIndex = kitten.db.posts.findIndex(post => post.id === data.remove)

    // Delete post if it exists.
    if (postIndex !== -1) {
      // Delete from database.
      kitten.db.posts.splice(postIndex, 1)

      // Remove the post from the interface.
      page.send(kitten.html`
        <${Post} post=${{id: data.remove}} remove />
      `)

      // Update post count in title.
      page.send(kitten.html`<${PostCountTitle} />`)
    }
  })
}
