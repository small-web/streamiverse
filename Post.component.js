function TrashIcon ({ title = 'Trash icon' }) {
  return kitten.html`
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentcolor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" role="img" aria-label="${title}">
      <polyline points="3 6 5 6 21 6"></polyline>
      <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
    </svg>
  `
}

export function RemoveButton ({ disabled = false, post, postId }) {
  const _postId = postId ?? post.id
  return kitten.html`
    <button
      id='remove-post-${_postId}'
      name='remove'
      connect
      ${disabled ? ['disabled'] : [`value=${post?.id}`]}
    >
      <${TrashIcon}
        title='Remove post by ${post?.account.display_name} ${disabled ? 'disabled' : ''}'
      />
    </button>
  `
}

export default function Post ({ post, showRemoveButton = false, remove = false }) {
  const id = () => `post-${post.id}`

  if (remove) {
    return kitten.html`
      <li id=${id()} hx-swap-oob='delete'></li>
    `
  }

  return kitten.html`
    <li id=${id()} class='Post component'>
      <${Avatar} post=${post} />
      <${Content} post=${post} />
      <if ${showRemoveButton}>
        <then>
          <${RemoveButton} post=${post} />
        </then>
      </if>

      <style>
        .Post {
          display: flex; align-items: flex-start; column-gap: 1em; padding: 1em;
          margin-bottom: 1em; background-color: var(--background-alt); border-radius: 1em;
        }
      </style>
    </li>
  `
}

// Private components (can only be used by Post).

const Avatar = ({ post }) => kitten.html`
  <a class='Avatar component' href='${post.url}'>
    <img src='${post.account.avatar}' alt='${post.account.username}’s avatar' />
  </a>
  <style>
    .Avatar img {
      width: 8em;
      border-radius: 1em;

      /* Tighten things up on narrow viewports. */
      @media (max-width: 560px) { width: 6em; }
      @media (max-width: 396px) { width: 4em; }
    }
  </style>
`

const Content = ({ post }) => kitten.html`
  <div class='Content component'>
    ${kitten.safelyAddHtml(post.content)}
    ${post.media_attachments.map(media => (
      (media.type === 'image' || media.type === 'gifv') && kitten.html`<img src='${media.remote_url || media.url}' alt='${media.description}'>`
    ))}
    <if ${post.card !== null}>
      <p>${[post.card?.html]}</p>
    </if>
  </div>
  <style>
    .Content { flex: 1; }
    .Content p:first-of-type { margin-top: 0; }
    .Content p { line-height: 1.5; }
    .Content a:not(.Avatar) {
      text-decoration: none; background-color: rgb(139, 218, 255, 0.5);
      border-radius: 0.25em; padding: 0.25em; color: black;
    }
    .Content img { width: 100%; max-width: 100%; }
    /* Make sure posts don’t overflow their containers. */
    .Content a {
      word-break: break-all;
    }
    /* Make cards appear full width; 16:9 aspect ratio. */
    iframe { width: 100%; height: auto; aspect-ratio: 16 / 9;}
  </style>
`
