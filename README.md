# Streamiverse

A curated stream of posts from the [fediverse](https://fedi.tips).

See it in action at [streamiverse.small-web.org](https://streamiverse.small-web.org).

Made with [Kitten](https://kitten.small-web.org)

## Hosted API

For educational and non-commercial uses, you can consume the feed and WebSocket updates from the Streamiverse instance hosted by [Small Technology Foundation](https://small-tech.org).

> If you do use our hosted version, please consider supporting [Small Technology Foundation](https://small-tech.org/fund-us/), our two-person not-for-profit working to make the [Small Web](https://small-tech.org/research-and-development/) happen.

### GET /public/

> https://streamiverse.small-web.org/public/

Returns the first ten posts as an array of [Mastodon API statuses](https://docs.joinmastodon.org/methods/statuses/#200-ok-1) in JSON format.

This is similar to the `/api/v1/timelines/public` call in the Mastodon API.

### WebSocket /stream.socket

> wss://streamiverse.small-web.org/stream.socket

Streams the curation of [Mastodon API statuses](https://docs.joinmastodon.org/methods/statuses/#200-ok-1) in JSON format.

This is similar to the `/api/v1/streaming?stream=public` WebSocket endpoint in the Mastodon API.

### Like this? Fund us!

We’re a tiny, independent not-for-profit. Your [patronage and donations](https://small-tech.org/fund-us) help us exist.
