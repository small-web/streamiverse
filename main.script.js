export default function () {
  // Create the database tables if they don’t already exist.
  if (kitten.db.posts === undefined) kitten.db.posts = []
  if (kitten.db.settings === undefined) kitten.db.settings = {}
}
